/* Benchmarks.cpp
*
* Copyright (C) 2006-2016 wolfSSL Inc.
*
* This file is part of wolfSSL.
*
* wolfSSL is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* wolfSSL is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1335, USA
*/


#include "stdafx.h"
#include "Benchmarks.h" /* contains include of Enclave_u.h which has wolfSSL header files */


/* Check settings of wolfSSL */
#if !defined(HAVE_AESGCM) || defined(NO_RSA) || defined(NO_SHA256)
#error please enable AES-GCM, RSA, and SHA256
#endif

/* Use Debug SGX ? */
#if _DEBUG
	#define DEBUG_VALUE SGX_DEBUG_FLAG
#else
	#define DEBUG_VALUE 1
#endif

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <time.h>
#include <stdio.h>
#include <dirent.h>

#include <opencv2/core/core.hpp>
#include "opencv2/imgcodecs.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/face.hpp>
#include "drawLandmarks.hpp"

using namespace std;
using namespace cv;
using namespace cv::face;

/* Choose AES Key size  */
//#define WC_AES_KEY_SZ 16 /* 128 bit key */
//#define WC_AES_KEY_SZ 24 /* 192 bit key */
#define WC_AES_KEY_SZ 32 /* 256 bit key */

#define SALT_SIZE 8


static double current_time(int reset)
{
	static int init = 0;
	static LARGE_INTEGER freq;
	LARGE_INTEGER count;

	(void)reset;

	if (!init) {
		QueryPerformanceFrequency(&freq);
		init = 1;
	}

	QueryPerformanceCounter(&count);
	return (double)count.QuadPart / freq.QuadPart;
}


static void free_resources(byte* plain, byte* cipher) {
	delete[] plain;
	delete[] cipher;
}


/* benchmark is performed calling into Enclave on each update
 * This function tests speeds at different message sizes during update */
static double sha256_getTime_multiple(sgx_enclave_id_t id, double* total) {
	double start, end;
	int ret, sgxStatus;
	byte* plain;
	byte digest[64];
	int plainSz = (1024 * 1024);
	int tSz = (1024 * 1024) * numBlocks;
	int i, k;

	Sha256 sha256;

	ret = 0;
	k = numBlocks;
	printf("\n");
	for (k = 1; k <= numBlocks; k++) {
		plainSz = tSz / k;
		plain = new byte[plainSz];
		ret |= wc_sha256_init(id, &sgxStatus, &sha256);
		start = current_time(1);
		for (i = 0; i < k; i++) {
			ret |= wc_sha256_update(id, &sgxStatus, &sha256, plain, plainSz);
		}
		ret |= wc_sha256_final(id, &sgxStatus, &sha256, digest);
		end = current_time(0);

		if (ret != SGX_SUCCESS || sgxStatus != 0) {
			printf("Error in SHA256 operation with Enclave: %d sgxStatus = %d.\n", ret, sgxStatus);
			return -1;
		}
		*total = end - start;
		printf("%8.3f\n", *total);
		delete[] plain;
	}
	printf("\n");
	*total = end - start;
	return 1 / *total * numBlocks;
}


/* benchmark is performed calling into Enclave on each update */
static double sha256_getTime(sgx_enclave_id_t id, double* total) {
	double start, end;
	int ret = 0;
	int sgxStatus = 0;
	int i;
	byte* plain;
	byte digest[64];
	int plainSz = (1024 * 1024);

	Sha256 sha256;

	plain = new byte[plainSz];
	ret |= wc_sha256_init(id, &sgxStatus, &sha256);
	start = current_time(1);
	
	/* perform work and get digest */
	for (i = 0; i < numBlocks; i++) {
		ret |= wc_sha256_update(id, &sgxStatus, &sha256, plain, plainSz);
	}
	ret |= wc_sha256_final(id, &sgxStatus, &sha256, digest);
	end = current_time(0);

	delete[] plain;
	if (ret != SGX_SUCCESS || sgxStatus != 0) {
		printf("Error in SHA256 operation with Enclave: %d sgxStatus = %d.\n", ret, sgxStatus);
		return -1;
	}

	*total = end - start;
	return 1 / *total * numBlocks;
}


static int sha256_print(sgx_enclave_id_t id)
{
	double total, persec;

	printf("SHA-256           ");
	persec = sha256_getTime(id, &total);
	printf("%d megs took %5.3f seconds , %8.3f MB/s\n", numBlocks, total, persec);

	return 0;
}

/* return time in MB/s with crossing into enclave boundary with each encrypt */


/*
* Makes a cyptographically secure key by stretching a user entered key
*/
/*
int GenerateKey(RNG* rng, byte* key, int size, byte* salt, int pad)
{
	int ret;

	ret = wc_RNG_GenerateBlock(rng, salt, SALT_SIZE);
	if (ret != 0)
		return -1020;

	if (pad == 0)
		salt[0] = 0;

	// /* stretches key 
	ret = wc_PBKDF2(key, key, strlen((const char*)key), salt, SALT_SIZE, 4096,
		size, SHA256);
	if (ret != 0)
		return -1030;

	return 0;
}
*/

/*
static double aesgcm_encrypt_file_getTime(sgx_enclave_id_t id, Aes* aes, byte* key, int size,  FILE* inFile, FILE* outFile)
{	
	RNG     rng;
	byte    iv[AES_BLOCK_SIZE];
	byte*   input;
	byte*   output;
	byte    salt[SALT_SIZE] = { 0 };

	int     i = 0;
	int     ret = 0;
	int     inputLength;
	int     length;
	int     padCounter = 0;

	fseek(inFile, 0, SEEK_END);
	inputLength = ftell(inFile);
	fseek(inFile, 0, SEEK_SET);

	length = inputLength;
	// /* pads the length until it evenly matches a block / increases pad number
	while (length % AES_BLOCK_SIZE != 0) {
		length++;
		padCounter++;
	}

	input = (byte*) malloc(length);
	output = (byte*) malloc(length);

	ret = wc_InitRng(&rng);
	if (ret != 0) {
		printf("Failed to initialize random number generator\n");
		return -1030;
	}

	// /* reads from inFile and wrties whatever is there to the input array 
	ret = fread(input, 1, inputLength, inFile);
	if (ret == 0) {
		printf("Input file does not exist.\n");
		return -1010;
	}
	for (i = inputLength; i < length; i++) {
		// /* padds the added characters with the number of pads 
		input[i] = padCounter;
	}

	ret = wc_RNG_GenerateBlock(&rng, iv, AES_BLOCK_SIZE);
	if (ret != 0)
		return -1020;

	// /* stretches key to fit size 
	ret = GenerateKey(&rng, key, size, salt, padCounter);
	if (ret != 0)
		return -1040;

	// /* sets key 
	ret = wc_AesSetKey(aes, key, AES_BLOCK_SIZE, iv, AES_ENCRYPTION);
	if (ret != 0)
		return -1001;

	// /* encrypts the message to the ouput based on input length + padding 
	ret = wc_AesCbcEncrypt(aes, output, input, length);
	if (ret != 0)
		return -1005;

	/* writes to outFile 
	fwrite(salt, 1, SALT_SIZE, outFile);
	fwrite(iv, 1, AES_BLOCK_SIZE, outFile);
	fwrite(output, 1, length, outFile);

	// /* closes the opened files and frees the memory
	memset(input, 0, length);
	memset(output, 0, length);
	memset(key, 0, size);
	free(input);
	free(output);
	free(key);
	fclose(inFile);
	fclose(outFile);
	wc_FreeRng(&rng);

	return ret;
	
}
*/

static double aes_encrypt_file(sgx_enclave_id_t id, FILE* inFile, FILE* outFile) {

	int ret = 0;

	Aes aes;
	byte iv[] =
	{
		0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xcd, 0xef,
		0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
		0x11, 0x21, 0x31, 0x41, 0x51, 0x61, 0x71, 0x81,
		0x11, 0x21, 0x31, 0x41, 0x51, 0x61, 0x71, 0x81
	};

	byte key[] =
	{
		0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
		0xfe, 0xde, 0xba, 0x98, 0x76, 0x54, 0x32, 0x10,
		0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67,
		0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67
	};

	byte*   input;
	byte*   output;

	int     inputLength;
	int     length;
	int     padCounter = 0;

	fseek(inFile, 0, SEEK_END);
	inputLength = ftell(inFile);
	fseek(inFile, 0, SEEK_SET);

	length = inputLength;
	/* pads the length until it evenly matches a block / increases pad number*/
	while (length % AES_BLOCK_SIZE != 0) {
		length++;
		padCounter++;
	}

	input = (byte*) malloc(length);
	output = (byte*) malloc(length);

	/* reads from inFile and wrties whatever is there to the input array */
	ret = fread(input, 1, inputLength, inFile);
	if (ret == 0) {
		printf("Input file does not exist.\n");
		return -1010;
	}
	for (int i = inputLength; i < length; i++) {
		/* padds the added characters with the number of pads */
		input[i] = padCounter;
	}



	int sgxStatus;

	
	//ret = wc_aesgcm_setKey(id, &sgxStatus, &aes, key, WC_AES_KEY_SZ);
	ret = wc_aes_setKey(id, &sgxStatus, &aes, key, AES_BLOCK_SIZE, iv, AES_ENCRYPTION);
	if (ret != SGX_SUCCESS || sgxStatus != 0) {
		printf("AES set key failed %d sgxStatus = %d\n", ret, sgxStatus);
		return -1;
	}	

	ret = wc_aescbc_encrypt(id, &sgxStatus, &aes, output, input, length);

	if (ret != 0) {
		printf("wc_AesCbcEncrypt failed.\n");
		//wolfCLU_freeBins(input, output, NULL, NULL, NULL);
		while (1);
		return ret;
	}

	fwrite(output, 1, length, outFile);

	memset(input, 0, length);
	memset(output, 0, length);
	//free(input);
	free(output);
	fclose(inFile);
	fclose(outFile);

	return ret;
}


static double aes_decrypt_file(sgx_enclave_id_t id, FILE* inFile, FILE* outFile) {

	int ret = 0;

	Aes aes;
	byte iv[] =
	{
		0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xcd, 0xef,
		0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
		0x11, 0x21, 0x31, 0x41, 0x51, 0x61, 0x71, 0x81,
		0x11, 0x21, 0x31, 0x41, 0x51, 0x61, 0x71, 0x81
	};

	byte key[] =
	{
		0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
		0xfe, 0xde, 0xba, 0x98, 0x76, 0x54, 0x32, 0x10,
		0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67,
		0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67
	};

	byte*   input;
	byte*   output;

	int     inputLength;
	int     length;
	int     padCounter = 0;

	fseek(inFile, 0, SEEK_END);
	inputLength = ftell(inFile);
	fseek(inFile, 0, SEEK_SET);

	length = inputLength;
	/* pads the length until it evenly matches a block / increases pad number*/
	while (length % AES_BLOCK_SIZE != 0) {
		length++;
		padCounter++;
	}

	input = (byte*)malloc(length);
	output = (byte*)malloc(length);

	/* reads from inFile and wrties whatever is there to the input array */
	ret = fread(input, 1, inputLength, inFile);
	if (ret == 0) {
		printf("Input file does not exist.\n");
		return -1010;
	}
	for (int i = inputLength; i < length; i++) {
		/* padds the added characters with the number of pads */
		input[i] = padCounter;
	}


	int sgxStatus;


	//ret = wc_aesgcm_setKey(id, &sgxStatus, &aes, key, WC_AES_KEY_SZ);
	ret = wc_aes_setKey(id, &sgxStatus, &aes, key, AES_BLOCK_SIZE, iv, AES_DECRYPTION);
	if (ret != SGX_SUCCESS || sgxStatus != 0) {
		printf("AES set key failed %d sgxStatus = %d\n", ret, sgxStatus);
		return -1;
	}

	ret = wc_aescbc_decrypt(id, &sgxStatus, &aes, output, input, length);
	if (ret != 0) {
		printf("wc_AesCbcDecrypt failed.\n");	
		while (1);
		return ret;
	}

	fwrite(output, 1, length, outFile);

	memset(input, 0, length);
	memset(output, 0, length);
	//free(input);
	free(output);
	fclose(inFile);
	fclose(outFile);

	return ret;
}

/* return time in MB/s with crossing into enclave boundary with each encrypt */
static double aesgcm_encrypt_getTime(sgx_enclave_id_t id, double* total, byte* plain, byte* cipher, word32 sz, byte* tag, word32 tagSz)
{
	Aes aes;
	double start, end;
	int i;
	int ret, sgxStatus;

	const byte ad[13] = { 0 };

	const XGEN_ALIGN byte iv[] =
	{
		0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xcd, 0xef,
		0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
		0x11, 0x21, 0x31, 0x41, 0x51, 0x61, 0x71, 0x81,
		0x11, 0x21, 0x31, 0x41, 0x51, 0x61, 0x71, 0x81
	};

	const XGEN_ALIGN byte key[] =
	{
		0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
		0xfe, 0xde, 0xba, 0x98, 0x76, 0x54, 0x32, 0x10,
		0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67,
		0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67
	};

	ret = wc_aesgcm_setKey(id, &sgxStatus, &aes, key, WC_AES_KEY_SZ);
	if (ret != SGX_SUCCESS || sgxStatus != 0) {
		printf("AES set key failed %d sgxStatus = %d\n", ret, sgxStatus);
		return -1;
	}

	start = current_time(1);
	for (i = 0; i < numBlocks; i++) {
		ret = wc_aesgcm_encrypt(id, &sgxStatus, &aes, cipher, plain, sz, iv, 12, tag, tagSz, ad, 13);
	}
	end = current_time(0);

	if (ret != SGX_SUCCESS || sgxStatus != 0) {
		printf("Error in AES-GCM encrypt operation with Enclave: %d sgxStatus = %d.\n", ret, sgxStatus);
		return -1;
	}

	*total = end - start;
	return 1 / *total * numBlocks;
}


static int aesgcm_encrypt_print(sgx_enclave_id_t id, byte* plain, byte* cipher, word32 sz, byte* tag, word32 tagSz)
{
	double total, persec;

	printf("AES-GCM encrypt   ");
	persec = aesgcm_encrypt_getTime(id, &total, plain, cipher, sz, tag, tagSz);
	printf("%d megs took %5.3f seconds , %8.3f MB/s\n", numBlocks, total, persec);

	return 0;
}


/* return MB/s with crossing into Enclave boundary with each decrypt */
static double aesgcm_decrypt_getTime(sgx_enclave_id_t id, double* total, byte* plain, const byte* cipher, word32 sz, const byte* tag, word32 tagSz)
{
	Aes aes;
	double start, end;
	int ret, sgxStatus;
	int i;

	const byte ad[13] = { 0 };

	const XGEN_ALIGN byte iv[] =
	{
		0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xcd, 0xef,
		0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
		0x11, 0x21, 0x31, 0x41, 0x51, 0x61, 0x71, 0x81,
		0x11, 0x21, 0x31, 0x41, 0x51, 0x61, 0x71, 0x81
	};

	const XGEN_ALIGN byte key[] =
	{
		0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
		0xfe, 0xde, 0xba, 0x98, 0x76, 0x54, 0x32, 0x10,
		0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67,
		0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67
	};

	ret = wc_aesgcm_setKey(id, &sgxStatus, &aes, key, WC_AES_KEY_SZ);
	if (ret != SGX_SUCCESS || sgxStatus != 0) {
		printf("AES set key failed %d sgxStatus = %d\n", ret, sgxStatus);
		return -1;
	}

	start = current_time(1);
	for (i = 0; i < numBlocks; i++) {
		ret = wc_aesgcm_decrypt(id, &sgxStatus, &aes, plain, cipher, sz, iv, 12, tag, tagSz, ad, 13);
	}
	end = current_time(0);

	if (ret != SGX_SUCCESS || sgxStatus < 0) {
		printf("Error in AES-GCM decrypt operation with Enclave: %d sgxStatus = %d.\n", ret, sgxStatus);
		return -1;
	}

	*total = end - start;
	return 1 / *total * numBlocks;
}


static int aesgcm_decrypt_print(sgx_enclave_id_t id, byte* plain, byte* cipher, word32 sz, const byte* tag, word32 tagSz)
{
	double total, persec;

	printf("AES-GCM decrypt   ");
	persec = aesgcm_decrypt_getTime(id, &total, plain, cipher, sz, tag, tagSz);
	printf("%d megs took %5.3f seconds , %8.3f MB/s\n", numBlocks, total, persec);

	return 0;
}


/* return time for each in milliseconds */
static double rsa_encrypt_getTime(sgx_enclave_id_t id, int* sgxStatus, double* total, const byte* message, word32 mSz, byte* cipher, word32 cSz)
{
	double start, end;
	int ret, i;
	int freeStatus = 0;
	RsaKey rsa;

	ret = 0; *sgxStatus = 0;
	ret = wc_rsa_init(id, sgxStatus, &rsa); /* loads RSA key from buffer and inits RNG */
	if (ret != SGX_SUCCESS || *sgxStatus != 0) {
		printf("Initializing RSA failed %d sgxStatus = %d\n", ret, *sgxStatus);
		return -1;
	}

	start = current_time(1);
	for (i = 0; i < ntimes; i++) {
		ret = wc_rsa_encrypt(id, sgxStatus, message, mSz, cipher, cSz, &rsa);
	}
	end = current_time(0);

	if (ret != SGX_SUCCESS || *sgxStatus < 0) {
		printf("Error in rsa encrypt operation with Enclave: %d sgxStatus = %d.\n", ret, *sgxStatus);
		return -1;
	}

	ret = wc_rsa_free(id, &freeStatus, &rsa);
	if (ret != 0 || freeStatus != 0) {
		printf("Failed to free RSA key %d sgxStatus = %d\n", ret, freeStatus);
		return -1;
	}

	*total = end - start;
	return (*total / ntimes) * 1000;
}


static int rsa_encrypt_print(sgx_enclave_id_t id, int* sgxStatus, byte* plain, word32 pSz, byte* cipher, word32 cSz)
{
	double total, each;

	printf("RSA-2048 encrypt   ");
	each = rsa_encrypt_getTime(id, sgxStatus, &total, plain, pSz, cipher, cSz);
	printf("took %6.3f milliseconds, avg over %d\n", each, ntimes);

	return 0;
}


/* return time in milliseconds for each */
static double rsa_decrypt_getTime(sgx_enclave_id_t id, double* total, byte* m, word32 mSz, const byte* c, word32 cSz)
{
	double start, end;
	int ret, sgxStatus, i;
	RsaKey rsa;

	ret = wc_rsa_init(id, &sgxStatus, &rsa); /* loads RSA key from buffer and inits RNG */
	if (ret != SGX_SUCCESS || sgxStatus != 0) {
		printf("Initializing RSA failed %d sgxStatus = %d\n", ret, sgxStatus);
		return -1;
	}

	start = current_time(1);
	for (i = 0; i < ntimes; i++) {
		ret = wc_rsa_decrypt(id, &sgxStatus, c, cSz, m, mSz, &rsa);
	}
	end = current_time(0);

	if (ret != SGX_SUCCESS || sgxStatus < 0) {
		printf("Error in rsa decrypt operation with Enclave: %d sgxStatus = %d.\n", ret, sgxStatus);
		return -1;
	}

	ret = wc_rsa_free(id, &sgxStatus, &rsa);
	if (ret != 0 || sgxStatus != 0) {
		printf("Failed to free RSA key %d sgxStatus = %d\n", ret, sgxStatus);
		return -1;
	}

	*total = end - start;
	return (*total / ntimes) * 1000;
}


static int rsa_decrypt_print(sgx_enclave_id_t id, byte* m, word32 mSz, const byte* c, word32 cSz)
{
	double total, each;

	printf("RSA-2048 decrypt   ");
	each = rsa_decrypt_getTime(id, &total, m, mSz, c, cSz);
	printf("took %6.3f milliseconds, avg over %d\n", each, ntimes);

	return 0;
}

//==============================================================================================
// Facial landmark Detection Code


void drawLandmarkDetection(Mat frame, Mat gray, CascadeClassifier faceDetector, Ptr<Facemark> facemark) {

	// Find face
	vector<Rect> faces;
	// Convert frame to grayscale because
	// faceDetector requires grayscale image.
	cvtColor(frame, gray, COLOR_BGR2GRAY);

	// Detect faces
	faceDetector.detectMultiScale(gray, faces);

	// Variable for landmarks. 
	// Landmarks for one face is a vector of points
	// There can be more than one face in the image. Hence, we 
	// use a vector of vector of points. 
	vector< vector<Point2f> > landmarks;

	// Run landmark detector
	bool success = facemark->fit(frame, faces, landmarks);

	if (success)
	{
		// If successful, render the landmarks on the face
		for (int i = 0; i < landmarks.size(); i++)
		{
			drawLandmarks(frame, landmarks[i]);
		}
	}

	// Display results 
	imshow("Facial Landmark Detection", frame);

}

int faceDetectionMain()
{

	String decryptedImgDir = "C:\\Users\\fs5ve\\Documents\\Visual Studio 2015\\Projects\\FaceDetectionEnclave\\images-decrypted\\";
	String dir = "C:\\Users\\fs5ve\\Documents\\Visual Studio 2015\\Projects\\FaceDetectionEnclave\\";
	// Set up webcam for video capture
	//VideoCapture cam(0); // change it with VideoCapture cap("C:\\Users\\fs5ve\\Documents\\visual studio 2015\\Projects\\facelandmarkdetection\\facelandmarkdetection\\myvideo.mp4");
	//VideoCapture cap(dir+"myvideo.mp4");

	//String files = dir + "images\\%06d.jpg";
	String files = decryptedImgDir + "%06d.jpg";
	VideoCapture cap(files); // for Image_001.png, etc


							 // Variable to store a video frame and its grayscale 
	Mat frame, gray;

	// Check if camera opened successfully
	if (!cap.isOpened()) {
		cout << "Error opening video stream or file" << endl;
		return -1;
	}

	//printf("Video loading done....\n");
	// Load Face Detector
	CascadeClassifier faceDetector(dir + "haarcascade_frontalface_alt2.xml");

	//printf("CascadeClassifier creation done....\n");

	// Create an instance of Facemark
	Ptr<Facemark> facemark = FacemarkLBF::create();

	//printf("CascadeClassifier creation done....\n");

	// Load landmark detector
	facemark->loadModel(dir + "lbfmodel.yaml");

	//printf("CascadeClassifier loading done....\n");

	// Read a frame	
	while (cap.read(frame))
	{
		//std::this_thread::sleep_for(std::chrono::milliseconds(2000));

		drawLandmarkDetection(frame, gray, faceDetector, facemark);
		// Exit loop if ESC is pressed
		if (waitKey(1) == 27) break;

	}
	return 0;
}


void fileBaseEncryption(sgx_enclave_id_t id, const char*in, const char *out) {
	//String dir = "C:\\Users\\fs5ve\\Documents\\Visual Studio 2015\\Projects\\FaceDetectionEnclave\\";
	//String files = dir + "images\\000001.jpg";

	FILE*  inFile;
	FILE*  outFile;

	//const char* in = "C:\\Users\\fs5ve\\Documents\\Visual Studio 2015\\Projects\\FaceDetectionEnclave\\encrypted\\000001.jpg";  //000001.jpg";
	//const char* out = "C:\\Users\\fs5ve\\Documents\\Visual Studio 2015\\Projects\\FaceDetectionEnclave\\encrypted\\01";

	int size = AES_256_KEY_SIZE;

	inFile = fopen(in, "rb");
	if (inFile)
	{
		printf("fopen success\n");
	}
	else 
	{
		printf("Error in opening file");
		return;
	}
	outFile = fopen(out, "wb");
	if (outFile)
	{
		printf("fopen success\n");
	}
	else
	{
		printf("Error in opening file");
		return;
	}
	//aesgcm_encrypt_file_getTime(sgx_enclave_id_t id, Aes* aes, byte* key, int size, FILE* inFile, FILE* outFile)
	//if (aesgcm_encrypt_file_getTime(id, &aes, key, size, inFile, outFile) != 0) {		
	if (aes_encrypt_file(id, inFile, outFile) != 0) {
		printf("Error occured in encyrpting file!!!!");
		getchar();
		
	}	
}

void fileBaseDecryption(sgx_enclave_id_t id, const char*in, const char *out) {
	FILE*  inFile;
	FILE*  outFile;


	inFile = fopen(in, "rb");
	if (inFile)
	{
		printf("fopen success\n");
	}
	else
	{
		printf("Error in opening file");
		return;
	}
	outFile = fopen(out, "wb");
	if (outFile)
	{
		printf("fopen success\n");
	}
	else
	{
		printf("Error in opening file");
		return;
	}

	if (aes_decrypt_file(id, inFile, outFile) != 0) {
		printf("Error occured in decyrpting file!!!!");
		getchar();

	}
}


char* concatTwoChar(char *line1, char *line2) {

	size_t len1 = strlen(line1);
	size_t len2 = strlen(line2);

	char *totalLine = (char*) malloc(len1 + len2 + 1);
	if (!totalLine) abort();

	memcpy(totalLine, line1, len1);
	memcpy(totalLine + len1, line2, len2);
	totalLine[len1 + len2] = '\0';

	return totalLine;
}

//==============================================================================================

int main(int argc, char* argv[])
{
	sgx_enclave_id_t id;
	sgx_launch_token_t t;

	const int tagSize = 16;
	int ret = 0;
	int sgxStatus = 0;
	int updated = 0;
	byte message[] = "fog mediation application bench";

	byte* plain  = new byte[1024 * 1024];
	byte* cipher = new byte[1024 * 1024];
	const byte tag[tagSize] = { 0 };
	int plainSz = 1024 * 1024;
	int i;

	/* only print off if no command line arguments were passed in */
	if (argc == 1) {
		printf("Setting up Enclave ... ");
	}

	memset(t, 0, sizeof(sgx_launch_token_t));

	ret = sgx_create_enclave(_T("Enclave.signed.dll"), DEBUG_VALUE, &t, &updated, &id, NULL);
	if (ret != SGX_SUCCESS) {
		printf("Failed to create Enclave : error %d - %#x.\n", ret, ret);
		free_resources(plain, cipher);
		return 1;
	}

	/* test if only printing off times */
	/*
	if (argc > 1) {
		double total;
		int idx = 1;
		while (1) {
			for (idx = 1; idx < argc; idx++) {
				if (strncmp(argv[idx], "-s256", 6) == 0) {
					printf("%8.3f - SHA-256\n", sha256_getTime(id, &total));
					fflush(stdout);
				}
				else if (strncmp(argv[idx], "-ag", 3) == 0) {
					printf("%8.3f - AES-GCM\n", aesgcm_encrypt_getTime(id, &total, plain, cipher, plainSz, (byte*)tag, sizeof(tag)));
					fflush(stdout);
				}
				else if (strncmp(argv[idx], "-re", 4) == 0) {
					printf("%8.3f - RSA-ENC\n", rsa_encrypt_getTime(id, &sgxStatus, &total, message, sizeof(message), cipher, plainSz));
					fflush(stdout);
				}
				else if (strncmp(argv[idx], "-rd", 4) == 0) {
					rsa_encrypt_getTime(id, &sgxStatus, &total, message, sizeof(message), cipher, plainSz);
					printf("%8.3f - RSA-DEC\n", rsa_decrypt_getTime(id, &total, plain, plainSz, cipher, sgxStatus));
					fflush(stdout);
				}
				else {
					printf("\"%s\" Not yet implemented\n\t-s256 for SHA256\n\t-ag for AES-GCM\n\t-re for RSA encrypt", argv[idx]);
					fflush(stdout);
					free_resources(plain, cipher);
					return 0;
				}
			}
		}
	}
	*/

	printf("Success\nCollecting benchmark values for wolfSSL using SGX\n");
	
    /*********** SHA-256 ***************/
	/*
	if (sha256_print(id) != 0) {
		free_resources(plain, cipher);
		return -1;
	}
	printf("\n");
	*/

	/*********** AES-GCM ***************/
	/* place message in first bytes of plain and test encrypt/decrypt with aesgcm */

	/*
	memcpy(plain, message, sizeof(message));

	if (aesgcm_encrypt_print(id, plain, cipher, plainSz, (byte*)tag, sizeof(tag)) != 0) {
		free_resources(plain, cipher);
		return -1;
	}

	printf("\tcipher message = ");
	for (i = 0; i < sizeof(cipher); i++) { printf("%c", cipher[i]); }
	printf("\n\n");

	//memcpy(cipher, message, sizeof(message));

	memset(plain, 0, plainSz);
	if (aesgcm_decrypt_print(id, plain, cipher, plainSz, tag, sizeof(tag)) != 0) {
		free_resources(plain, cipher);
		return -1;
	}

	printf("\tdecrypted message = ");
	for (i = 0; i < sizeof(message); i++) { printf("%c", plain[i]); }
	printf("\n\n");

	*/

	/*********** RSA ***************/
	/*
	memset(cipher, 0, 256);
	ret = rsa_encrypt_print(id, &sgxStatus, message, sizeof(message), cipher, plainSz);
	if (ret < 0) {
		free_resources(plain, cipher);
		return -1;
	}

	memset(plain, 0, plainSz);
	ret = rsa_decrypt_print(id, plain, plainSz, cipher, sgxStatus);
	if (ret != 0) {
		free_resources(plain, cipher);
		return -1;
	}

	printf("\tdecrypted message = ");
	for (i = 0; i < 15; i++){ printf("%c", plain[i]); }
	printf("\n");

	*/
	/*********** Free arrays and exit ***************/
	free_resources(plain, cipher);


	printf("\t Finishing Encryption-Decryption Process ");


	printf("\t Starting Face Landmark Detection Process ");
	clock_t tStart = clock();



	printf("\t Finishing Face Landmark Detection Process ");
	printf("Time taken For Face Landmark Detection: %.2fs\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);


	/* File Encrypt Decrypt*/
	
	//Encryption

	DIR *dir1;
	struct dirent *ent1;
	char *imgDir = "C:\\Users\\fs5ve\\Documents\\Visual Studio 2015\\Projects\\FaceDetectionEnclave\\images\\";
	char *encDir = "C:\\Users\\fs5ve\\Documents\\Visual Studio 2015\\Projects\\FaceDetectionEnclave\\encrypted\\";
	int count = 0;
	if ((dir1 = opendir(imgDir)) != NULL) {
		/* print all the files and directories within directory */
		while ((ent1 = readdir(dir1)) != NULL) {
			//printf("%s\n", ent->d_name);
			//string totalLine = imgDir + (string) ent->d_name;
			char * imageFileName = concatTwoChar(imgDir, ent1->d_name);
			printf("%s\n", imageFileName);

			char *encFileName = concatTwoChar(encDir, ent1->d_name);
			//printf("%s\n", encFileName);
			count++;

			fileBaseEncryption(id, (const char *)imageFileName, (const char *)encFileName);
		}
		closedir(dir1);
	}
	else {
		/* could not open directory */
		perror("");
		return EXIT_FAILURE;
	}


	//Decryption
	DIR *dir;
	struct dirent *ent;
	char *outputDir = "C:\\Users\\fs5ve\\Documents\\Visual Studio 2015\\Projects\\FaceDetectionEnclave\\images-decrypted\\";
	char *encImgDir = "C:\\Users\\fs5ve\\Documents\\Visual Studio 2015\\Projects\\FaceDetectionEnclave\\encrypted\\";
	count = 0;
	if ((dir = opendir(encImgDir)) != NULL) {
		/* print all the files and directories within directory */
		while ((ent = readdir(dir)) != NULL) {
			//printf("%s\n", ent->d_name);
			//string totalLine = imgDir + (string) ent->d_name;
			char * imageFileName = concatTwoChar(outputDir, ent->d_name);
			printf("%s\n", imageFileName);



			char *encFileName = concatTwoChar(encImgDir, ent->d_name);
			printf("%s\n", encFileName);
			count++;

			fileBaseDecryption(id, (const char *)encFileName, (const char *)imageFileName);
		}
		closedir(dir);
	}
	else {
		/* could not open directory */
		perror("");
		return EXIT_FAILURE;
	}

	


	//const char* in = "C:\\Users\\fs5ve\\Documents\\Visual Studio 2015\\Projects\\FaceDetectionEnclave\\encrypted\\000001.jpg";  //000001.jpg";
	//const char* out = "C:\\Users\\fs5ve\\Documents\\Visual Studio 2015\\Projects\\FaceDetectionEnclave\\encrypted\\01";

	//fileBaseEncryption(id, in, out);

	//const char* in1 = "C:\\Users\\fs5ve\\Documents\\Visual Studio 2015\\Projects\\FaceDetectionEnclave\\encrypted\\000002.jpg";
	//char* out1 = "C:\\Users\\fs5ve\\Documents\\Visual Studio 2015\\Projects\\FaceDetectionEnclave\\images-decrypted\\000002.jpg";
	//fileBaseDecryption(id, in1, out1);
	

	/* FaceDetection Code Block from main*/
	printf("Face Detection Started \n");

	//getchar();

	tStart = clock();

	faceDetectionMain();

	printf("Time taken for facedetection: %.2fs\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);

	getchar();
	return 0;
}

